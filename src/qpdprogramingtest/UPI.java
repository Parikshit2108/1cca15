package qpdprogramingtest;

public interface UPI {

    void makePayment();
    void checkBalance();
}
