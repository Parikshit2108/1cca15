package qpdprogramingtest;

public interface OnlineBanking {
    void transferAmount();
    void creditAmount();
}
