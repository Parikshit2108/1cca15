package qpdprogramingtest;

public class Main1 {
    public static void main(String[] args) {
        Machine m1= new Laptop(); // upcasting
        m1.machinePrice(2500);
        m1.machineType();

        Laptop l1=(Laptop)m1 ; // down casting
        l1.machineType();
        l1.machinePrice(3000);

    }
}
