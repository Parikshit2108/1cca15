package qpdprogramingtest;

public class PhonePe implements OnlineBanking,UPI{
    @Override
    public void transferAmount() {
        System.out.println("Amount Transfer Successfully");
    }

    @Override
    public void creditAmount() {
        System.out.println(" Amount Credited Successfully");
    }

    @Override
    public void makePayment() {
        System.out.println("Payment done Successfully");
    }

    @Override
    public void checkBalance() {
        System.out.println("Account Balance is  RS 1000");
    }
}
