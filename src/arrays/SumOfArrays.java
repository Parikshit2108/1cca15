package arrays;

import java.util.Scanner;

public class SumOfArrays {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Total Numbers of values to Sum");
        int values = sc1.nextInt();
        int[] num = new int[values];
        int sum=0;
        System.out.println("Enter The values");
        for (int a=0; a < num.length; a++) {
            num[a] = sc1.nextInt();
            sum+=num[a];
        }
        System.out.println(sum);
    }
}
