package arrays;

import java.util.Scanner;

public class LargerSmallerArrayValue {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter the array Length");
        int n1 = sc1.nextInt();
        int [] arr=new int[n1];
        System.out.println("Enter array values");
        for(int i=0; i<arr.length;i++){
            arr[i]= sc1.nextInt();
        }
        int large=arr[0];
        int small=arr[0];
        for(int j=0; j<arr.length;j++) {
            if (arr[j] > large)
                large = arr[j];
            if (arr[j] < small)
                small = arr[j];
        }
        System.out.println("largest number\t"+large+" "+"smallest number\t"+small);
    }
}
