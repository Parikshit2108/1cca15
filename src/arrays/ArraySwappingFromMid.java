package arrays;

public class ArraySwappingFromMid {
    public static void main(String[] args) {
        int [] arr=RandomArrayClass.randomArr(6);
        for (int a:arr){
            System.out.print(a+" ");
        }
        int count= arr.length/2;
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[count];
            arr[count] = temp;
            count++;
        }
        System.out.println();
        for(int a:arr){
            System.out.print(a+" ");
    }
}
}
