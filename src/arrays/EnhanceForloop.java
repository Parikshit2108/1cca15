package arrays;
public class EnhanceForloop {
    public static void main(String[] args) {
        int[] num = {2,4,6,8,10,12,14,16,18,20};
        int sum=0;
        for(int a:num){
            sum+=a;
        }
        System.out.println("Sum of array values "+sum);
        System.out.println("Average Of sum " + sum/num.length);
    }
}
