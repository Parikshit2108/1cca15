package arrays;

import java.util.Arrays;

public class DynamicArrayMain {
    public static void main(String[] args) {
        DynamicArray numbers = new DynamicArray(3);
        numbers.insert(10);
        numbers.insert(20);
        numbers.insert(30);
        numbers.insert(40);
        try {
            numbers.removeAt(5);

        } catch (IllegalArgumentException l) {
            System.out.println(l);
        }
        numbers.print();

        System.out.println("index of 10 is " + numbers.indexof(10));
    }
}
