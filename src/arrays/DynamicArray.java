package arrays;
public class DynamicArray {
    private int [] items;
    private int count;
    public DynamicArray( int length) {// parametrized constructor creation
        items=new int[length]; // through parameter defining the length of array
    }
    public void insert(int item) {   // create a method for array value insertion
        // if the array is full resize it
        if (items.length == count) {
            int[] newItems = new int[count * 2];//big O(n)
            for (int i = 0; i < count; i++)
                //copy all the existing items
                newItems[i] = items[i];
            //set "items" to new array
            items = newItems;
        }
        items[count++] = item;
    }
    public void  removeAt(int index) {
        if (index < 0 || index >= count)
            throw new IllegalArgumentException(" ArrayIndexOutOFBound");
        //shifting the items to left to fill the blank space  //time complexity big o(1) finding index with one line pf execution
        for (int i = index; i < count; i++) {
            items[i] = items[i + 1];
            count--;
        }
    }
    public int indexof(int item) {
        for (int i = 0; i < count; i++) {
            if (items[i] == item)
            return i;
        }
        return -1;
    }
        public void print() {
            for (int i = 0; i < count; i++)
                System.out.println(items[i]);
        }
    }


