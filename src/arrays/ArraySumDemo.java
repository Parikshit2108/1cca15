package arrays;

import java.util.Scanner;

public class ArraySumDemo {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER THE LENGTH OF FIRST ARRAY");
        int l1 = sc1.nextInt();
        int[] arr1 = new int[l1];
        System.out.println("ENTER THE LENGTH OF SECOND ARRAY");
        int l2 = sc1.nextInt();
        int[] arr2 = new int[l2];
        System.out.println("ENTER VALUES FOR ARRAY ONE");
        for (int i = 0; i < l1; i++) {
            arr1[i] = sc1.nextInt();
        }
        System.out.println("ENTER VALUES FOR ARRAY SECOND");
        for (int j = 0; j < l2; j++) {
            arr2[j] = sc1.nextInt();
        }

            int[] arr3 = new int[arr1.length> arr2.length? arr1.length : arr2.length];
            for (int k = 0; k < arr3.length; k++) {
                int num1=k<l1?arr1[k]:0;
                int num2=k<l2?arr2[k]:0;
                arr3[k] = num1+num2;
            }
        for (int i : arr3) {
            System.out.println("Sum of  Array\t" + i);
        }
            System.out.println(arr3.length);
    }
}
