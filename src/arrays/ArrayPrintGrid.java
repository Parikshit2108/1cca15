package arrays;

import java.util.Scanner;

public class ArrayPrintGrid {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Array Length 1:");
        int l1= sc1.nextInt();
        System.out.println("Enter Array Length 2:");
        int l2= sc1.nextInt();
        int [][] arr1= new int[l1][l2];
        for(int i=0; i<l1-1;i++){
            for (int j=0 ; j<l2-1;j++){
                System.out.print(arr1[i][j]);
            }
            System.out.println();
        }

    }
}
