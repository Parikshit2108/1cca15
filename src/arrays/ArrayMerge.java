package arrays;

import java.util.Arrays;

public class ArrayMerge {
    public static void main(String[] args) {
        int [] arr1 ={1,2,3,4};
        int  []arr2={5,6,7,8,9};
        int []arr3= new int[arr1.length+ arr2.length];
        for(int i=0;i< arr1.length;i++){
            arr3[i]=arr1[i];
        }
        int ch=4;
        for (int j : arr2) {
            arr3[ch] = j;
            ch++;
        }
        System.out.print(Arrays.toString(arr3));
       /* for(int a:arr3) {
            System.out.print(a);
        }*/
    }
}
