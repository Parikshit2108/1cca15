package arrays;

import java.util.Random;

public class RandomArrayClass {
   static Random rd= new Random();
   public static  int[]randomArr(int n) {
       int[] arr = new int[n];

       for(int a=0;a<n;a++){
           arr[a]= rd.nextInt(50);
       }
       return arr;
    }
}
