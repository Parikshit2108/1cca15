package arrays;

import java.util.Arrays;

public class ArrayMerge2 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4};
        int[] arr2 = {5, 6, 7, 8};
        int[] arr3 = new int[arr1.length + arr2.length];
        int k=0;
        for (int value : arr1) {
            arr3[k] = value;
            k += 2;
        }
        int ch=1;
        for (int j : arr2) {
            arr3[ch] = j;
            ch+=2;
        }
        System.out.print(Arrays.toString(arr3));
    }
}
