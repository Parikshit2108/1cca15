package arrays;

import java.util.Scanner;

public class AverageOfArrayValues {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter Total Numbers of values to get Average");
        int values = sc1.nextInt();
        int[] num = new int[values];
        int sum = 0;
        System.out.println("Enter The values");
        int average = 0;
        for (int a = 0; a < num.length; a++) {
            num[a] = sc1.nextInt();
            sum += num[a];
            average = sum/num.length;
        }

        System.out.println("AverageOfArrayValues\t" + average);
    }
}
