package arrays;

public class ArrayThirdSmallerValue {
    public static void main(String[] args) {
        int [] arr=RandomArrayClass.randomArr(10);
        for (int a:arr){
            System.out.print(a+" ");
        }
        int s1=arr[0];
        int s2=0;
        for (int a:arr) {
            if (a<s1) {
                s1=a;
                s2=s1;
            }
        }
        System.out.println("smallest number is "+s1);

    }
}

