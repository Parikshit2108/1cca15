package arrays;

public class ArraySwaping {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        for (int i : arr) {
            System.out.print(i);
        }
        int count = arr.length - 1;
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[count];
            arr[count] = temp;
            count--;
        }
        System.out.println();
        for (int i : arr) {
            System.out.print(i);
        }
    }
}