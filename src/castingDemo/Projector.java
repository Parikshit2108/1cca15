package castingDemo;

public class Projector extends Machine{
    @Override
    void getType() {
        System.out.println("Machine type is projector");
    }

    @Override
    void calculateBill(int qty, double price) {
        //10% gst add on bill
        double total = qty*price;
        double finalTotal=total+total*0.10;
        System.out.println("Final Total Amount is "+finalTotal);
    }
}
