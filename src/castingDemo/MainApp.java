package castingDemo;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter Product Qty");
        int qty= sc1.nextInt();
        System.out.println("Enter Product Price");
        double price= sc1.nextDouble();
        System.out.println("Select Product choice");
        System.out.println("1: for Laptop\t 2: for Projector");
        int choice= sc1.nextInt();
        Machine m1= null;
        if (choice==1){
            m1=new Laptop();
        } else if (choice==2) {
            m1= new Projector();
        }else{
            System.out.println("Invalid choice");
        }
        try {
            m1.getType();
            m1.calculateBill(qty, price);
        }catch (NullPointerException e){
            System.out.println(e);

        }
    }
}
