package castingDemo;


public class UpcastingMainA1 {
    public static void main(String[] args) {
        UpcastingSample s1= new UpcastingSample();
        s1.info();
        s1.info();
        s1.display();
        //upcasting
        System.out.println("=====Upcasting ======");
        UpcastingDemo d1=new UpcastingSample();
        d1.test();// only super class properties are accesseble
        d1.display();
    }
}
