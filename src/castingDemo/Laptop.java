package castingDemo;

public class Laptop extends Machine{
    @Override
    void getType() {
        System.out.println("Machine type is laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        //15% gst add on bill
        double total = qty*price;
        double finalTotal=total+total*0.15;
        System.out.println("Final Total Amount is "+finalTotal);
    }
}
