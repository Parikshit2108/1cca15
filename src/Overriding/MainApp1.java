package Overriding;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty= sc1.nextInt();
        System.out.println("Enter Price");
        double price= sc1.nextInt();
        System.out.println("Select choice");
        System.out.println("1: Flipkart\n2:Amazon");
        double choice= sc1.nextInt();
        if (choice==1){
            Flipkart f1= new Flipkart();
            f1.sellProduct(qty,price);
        } else if (choice==2) {
            Amazon a1= new Amazon();
            a1.sellProduct(qty,price);
        }else{
            System.out.println("Invalid Choice");
        }
    }
}
