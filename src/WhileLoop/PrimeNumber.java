package WhileLoop;

import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter number");
        int a=sc1.nextInt();
        int count=0;
        for(int i=1; i<=a;i++){
            if(a%i==0){
                count++;
            }
        }
        if(count==2)
            System.out.println("number is prime");
        else
            System.out.println("number is not prime ");
    }
}
