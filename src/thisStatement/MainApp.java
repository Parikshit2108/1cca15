package thisStatement;

public class MainApp {
    public static void main(String[] args) {
        System.out.println("Main Started");
        Central c1= new Central();
        c1.info();
        System.out.println("Main Ended");
    }
}
