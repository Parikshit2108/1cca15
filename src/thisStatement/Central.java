package thisStatement;

public class Central extends Master {
    Central(){
        super('J');
        System.out.println("Subclass Constructor");
    }
    void info(){
        System.out.println("Subclass Method");
    }
}
