package patternTest;

public class StarPattern2 {
    public static void main(String[] args) {
        int lines =9;
        int rows =1;
        int star=1;

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j <star; j++) {
                System.out.print("*");
            }
            if (i < 4) {
                star++;
            } else {
                star--;
            }
            System.out.println();
        }
    }
}
