package patternTest;

public class NumberTriangle {
    public static void main(String[] args) {
        int lines=5;
        int ch=1;
        int space=4;
        int rows=1;

        for(int i=0;i<lines;i++) {
            for (int j = 0; j <space; j++) {
                    System.out.print(" ");
                }
            for (int j = 0; j <rows; j++) {
                System.out.print(ch +" ");
                ch++;
                if(ch>6){
                    ch=1;}
            }
            space--;
            rows++;
            System.out.println();
            }

         }
    }
