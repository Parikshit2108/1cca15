package patternTest;

public class StarPattern1 {
    public static void main(String[] args) {
        int lines=6;
        int star=1;
        int space=0;

        for(int i=0;i<lines;i++) {
            for (int j =0; j<space; j++) {
                System.out.print(" ");

            }
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            star++;
            space++;
            System.out.println();
        }

    }

}

