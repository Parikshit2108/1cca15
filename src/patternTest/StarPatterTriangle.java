package patternTest;

public class StarPatterTriangle {
    public static void main(String[] args) {
        int lines=5;
        int star=1;
        int space=4;

        for(int i=0;i<lines;i++) {
            for (int j = 0; j <space; j++) {
                if (i+j==4 || i == 4) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("*");
            System.out.println();
        }

    }
}
