package patternTest;

public class CharPattern {
    public static void main(String[] args) {
        int lines = 5;
        int rows = 5;
        char ch = 'A';

        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(ch);
            }
            if (i < 2) {
                ch++;
            } else {
                ch--;
            }
            System.out.println();
        }
    }
}
