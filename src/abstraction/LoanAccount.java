package abstraction;

public class LoanAccount implements Account{
    double loanAmount;
    // account creation
    public LoanAccount(double accountBalance) {
        this.loanAmount= accountBalance;
        System.out.println("Loan account is Created");
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"RS  Debited From Your Loan Account");

    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+"RS Credited To Your Loan Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance Is"+loanAmount);

    }
}
