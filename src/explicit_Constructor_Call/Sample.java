package explicit_Constructor_Call;

public class Sample extends Demo {
    public Sample() {
        super(25); // declared by programmer
        System.out.println("subclass constructor");
    }
}