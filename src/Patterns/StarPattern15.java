package Patterns;

public class StarPattern15 {
    public static void main(String[] args) {
        int line = 9;
        int star = 5;
        int space =0;
        int i;
        for (i = 0; i < line; i++) {
            for (int k = 0; k < space; k++)
                System.out.print(" ");
            for (int j = 0; j < star; j++)
                if (j == 0) {
                    System.out.print(" ");
                } else System.out.print("*");
            System.out.println();
            if (i <= 3)
                space++;
            else
                space--;
        }
    }
}
