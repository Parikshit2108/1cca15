package Patterns;

public class NumberPattern17 {
    public static void main(String[] args) {
        int line=5;
        int num=1;
        char ch='A' ;
        for (int i=0; i<line;i++){
            for (int j=0; j<num;j++){
                System.out.print(ch);
            }
            System.out.println();
            num++;
            ch++;
        }
    }
}
