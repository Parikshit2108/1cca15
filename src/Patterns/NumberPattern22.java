package Patterns;

public class NumberPattern22 {
    public static void main(String[] args) {
        int line=7;
        int num=1;
        int ch=3;
        for(int i=0; i<line; i++) {
            int ch1=ch;
            for (int j = 0; j < num; j++) {
                System.out.print(ch1);
                ch1++;
            }
            System.out.println();
            if (i < 3){
                num++;
                ch-=1;
            }else{
                num--;
                ch++;}
        }
    }
}
