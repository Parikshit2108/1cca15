package Patterns;

public class StarPatter11 {
    public static void main(String[] args) {
        int line = 7;
        int num = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print("* ");
            }
            System.out.println();
            if(i<3){
                num++;
            }else{
                num--;
            }
        }
    }
}

