package Patterns;

public class NumberPattern3 {
    public static void main(String[] args) {
        int line=3;
        int star=2;
        char ch='A';
        for (int i=0;i<line;i++) {
            for (int j=0;j<star; j++){
                System.out.print(ch++);
            }
            System.out.println(ch++);
        }
    }

}
