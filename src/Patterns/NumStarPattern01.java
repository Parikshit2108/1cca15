package Patterns;

public class NumStarPattern01 {
    public static void main(String[] args) {
        int line = 9;
        int num = 1;
        int ch=1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < num; j++) {
                if(j%2==0) {
                    System.out.print("*");
                }else{
                    System.out.print(ch);
                }
            }
            System.out.println();
            if(i<4){
                num++;
            }else{
                num--;
            }

        }

    }
}
