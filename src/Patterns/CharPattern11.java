package Patterns;

public class CharPattern11 {
    public static void main(String[] args) {
        int line = 7;
        int num = 1;
        char ch='A';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(ch);
            }
            System.out.println();
            if(i<3){
                num++;
            }else{
                num--;
            }
            ch++;
        }
    }
}
