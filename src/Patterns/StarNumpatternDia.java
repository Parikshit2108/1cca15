package Patterns;

public class StarNumpatternDia {
    public static void main(String[] args) {
        int line = 3;
        int star = 1;
        int space = 2;
        int ch = 1;
        int i;
        for (i = 0; i < line; i++) {
            for (int k = 0; k < space; k++)
                System.out.print(" ");
            for (int j = 0; j < star; j++)
                if (j == i) {
                    System.out.print(ch);
                }
            else System.out.print("*");
            System.out.println();
            ch++;
            star+=2;
            space--;
        }

    }
}

