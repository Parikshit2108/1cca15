package Patterns;

public class NumberPattern11 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        char ch='A';
        int num=1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if (i% 2 == 0 ) {
                    System.out.print(num++);
                } else {
                    System.out.print(ch++);
                }
            }
           num =1;
            ch='A';
            System.out.println();
        }
    }
}
