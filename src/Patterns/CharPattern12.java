package Patterns;

public class CharPattern12 {
    public static void main(String[] args) {
        int line = 5;
        int num = 1;
        char ch='A';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(ch);
            }
            System.out.println();
            if(i<2){
                num++;
            }else{
                num--;
            }
            ch+=2;
        }
    }
}
