package Patterns;

public class StarPattern16 {
    public static void main(String[] args) {
        int line =9;
        int star=9;
        int space=0;

        for (int i=0; i<line; i++){
            for (int j=0; j<space;j++)
                System.out.print(" ");
            for(int k=0; k<=star;k++)
                if(k==0)
                    System.out.print(" ");
            else
                    System.out.print("*");
            System.out.println();
            if(i<4) {
                space++;
                star -= 2;
            }else{
                space--;
                star += 2;
            }
        }
    }
}
