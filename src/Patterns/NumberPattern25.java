package Patterns;

public class NumberPattern25 {
    public static void main(String[] args) {
        int line = 5;
        int rows = 1;

        int ch =1;
        for(int i=1;i<=line;i++){
            int num=i;
            for(int j=1;j<=rows;j++){
                System.out.print(num+" ");
                num+=line-j;
            }
            System.out.println();
            rows++;
        }

        int n = 5;

        //Loop to iterate over each row
        for (int i = 1; i <= n; i++) {
            //Loop to iterate over each column
            int counter = i;
            for (int j = 1; j <= i; j++) {
                System.out.print(counter + " ");
                counter = counter + n - j;
            }
            System.out.println();
        }
    }
}

