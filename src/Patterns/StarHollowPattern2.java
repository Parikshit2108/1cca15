package Patterns;

public class StarHollowPattern2 {
    public static void main(String[] args) {
        int line =9;
        int row=9;
        int star=1;
        int star1=1;
        int space=8;
        for(int i=0;i<line;i++) {
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            for(int k=0; k<space;k++) {
                System.out.print(" ");
            }
            for(int l=0;l<star1;l++){
                System.out.print("*");
            }
            System.out.println();
            if (i<4) {
                star++;
                space-=2;
                star1++;

            }else{
                star--;
                space+=2;
                star1--;
            }
        }
    }
}
