package Patterns;

public class NumberPattern16 {
    public static void main(String[] args) {
        int line=5;
        int num=1;
        char ch='A';
        for (int i=0; i<line;i++){
            for (int j=0; j<num;j++){
                System.out.print( ch );
                ch++;
            }
            System.out.println();
            ch='A';
            num++;
        }
    }
}
