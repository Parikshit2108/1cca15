package Patterns;

public class NumberPattern20 {
    public static void main(String[] args) {
        int line=4;
        int num=4;
        int ch=1;
        for (int i=0; i<line;i++){
            for (int j=0; j<num;j++){
                System.out.print(ch);
                ch++;
            }
            System.out.println();
            num--;
        }
    }
}
