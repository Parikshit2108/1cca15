package Patterns;

public class NumberPattern24 {
    public static void main(String[] args) {
        int line=7;
        int space=3;
        int star=0;
        for(int i=0; i<line; i++) {
            int ch=i;
            for (int j = 0; j <space; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k <star; k++) {
                if(i>k)
                    System.out.print(ch--);
                else
                    System.out.print(ch++);
            }
            System.out.println();
            ch+=2;
                    space--;
        }
    }

}
