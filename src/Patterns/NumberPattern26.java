package Patterns;

public class NumberPattern26 {
    public static void main(String[] args) {
        int line = 5;
        int rows = 1;
        for(int i=1;i<=line;i++) {
            int num =1;
            for (int j = 1; j <= rows; j++) {
                System.out.print(num + " ");
                num++;
            }
            num-=1;
            for(int k=1; k<rows;k++){
                System.out.print(num-1+" ");
                num--;
            }
            System.out.println();
            rows++;
        }
    }
}
