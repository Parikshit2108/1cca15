package Patterns;

public class NumberPattern18 {
    public static void main(String[] args) {
        int line=5;
        int num=1;
        int ch=1 ;
        for (int i=0; i<line;i++){
            for (int j=0; j<num;j++){
                System.out.print(ch);
            }
            System.out.println();
            num++;
            ch++;
        }
    }
}
