package Patterns;

public class NumberPAttern4 {
    public static void main(String[] args) {
        int line=3;
        int star=4;

        for (int i=0;i<line;i++) {
            char A=65;
            for (int j=0;j<star; j++){
                System.out.print(A++);
            }
            System.out.println();
        }
    }

}
