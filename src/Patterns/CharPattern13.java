package Patterns;

public class CharPattern13 {

    public static void main(String[] args) {
        int line = 9;
        int num = 1;
        char ch='A';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(ch++);
                if (ch>'E'){
                    ch='A';
                }
            }
            System.out.println();
            if(i<4){
                num++;
            }else{
                num--;
            }

        }
    }
}
