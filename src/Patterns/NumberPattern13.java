package Patterns;

public class NumberPattern13 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch=1;
        for (int i = 0; i < line; i++) {
                ch=i+1 ;
            for (int j = 0; j < star; j++) {
                System.out.print(ch + " ");
                ch += 5;
            }
            System.out.println();
        }
    }
}
