package Patterns;

public class NumberPattern21 {
    public static void main(String[] args) {
        int line=7;
        int num=1;
        int ch=3;
        for(int i=0; i<line; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(ch);
                ch--;
            }

            System.out.println();
            if (i < 3){
                num++;
            ch = 3;
        }else{
            num--;
            ch=3;}
        }
    }
}
