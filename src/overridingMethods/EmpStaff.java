package overridingMethods;

public class EmpStaff extends Employee {
    @Override
    void calculateSallary(double workingDays, double dailyWages) {
        //super.calculateSallary(workingDays, dailyWages);
        double monthlySal=workingDays*dailyWages;
        System.out.println("MONTHLY SALLARY IS\t"+ monthlySal);
    }
}
