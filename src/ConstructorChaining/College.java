package ConstructorChaining;

public class College extends University{
    public College(String universityName, String collegeName) {
        super(universityName);
        System.out.println("College "+ collegeName);
    }
}
