package ConstructorChaining;

public class Department extends College{
    public Department(String universityName, String collegeName,String department) {
        super(universityName, collegeName);
        System.out.println("Department "+department);
    }
}
