package ConstructorChaining;

public class University {
    public University(String universityName) {
        System.out.println("University "+ universityName);
    }
}
