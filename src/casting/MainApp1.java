package casting;
import java.util.Scanner;
public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select Service Provider");
        System.out.println("1:AirAsia\n2:Luftansa");
        int choice = sc1.nextInt();
        if (choice == 1 || choice == 2) {
            System.out.println("Select route");
            System.out.println("0: for MUMBAI TO PUNE");
            System.out.println("1: for PUNE TO BENGALURU");
            System.out.println("2: for BENGALURU TO CHENNAI");
            int routeChoice = sc1.nextInt();
            System.out.println("Enter Tickets Qty");
            int qty = sc1.nextInt();
            Goibibo g1 = null;
            if (choice == 1) {
                g1 = new AirAsia();
            } else if (choice == 2) {
                g1 = new Luftansa();
            } else {
                System.out.println("Invalid choice");
            }
            g1.bookTickets(qty, routeChoice);
        }else{
            System.out.println("Invalid Choice");
        }
    }
}
