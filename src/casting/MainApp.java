package casting;

public class MainApp {
    public static void main(String[] args) {
        TV t1 ;
        t1= new LED();
        t1.displayPicture();

        t1= new LCD();
        t1.displayPicture();
    }
}
