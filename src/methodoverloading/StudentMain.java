package methodoverloading;

import java.util.Scanner;

public class StudentMain {
    public static void main(String[] args) {
        Student s1 = new Student();
        Scanner sc1= new Scanner(System.in);
        System.out.println("Select search option");
        System.out.println("1: Search by Name");
        System.out.println("2: Search by contact");
        int choice= sc1.nextInt();
        if(choice==1){
            System.out.println("Enter Name");
            String sname= sc1.next();
            s1.search(sname);
        }else if(choice==2){
            System.out.println("Enter Contact No");
            long contact= sc1.nextLong();
            s1.search(contact);
        }else{
            System.out.println("Invalid choice");
        }
    }
}
