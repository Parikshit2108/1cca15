package collectionframework;

public class Book {
    int id;
    String bookName;
    double bookPrice;
    public Book(int id, String bookName, double bookPrice) {
        this.id = id;
        this.bookName = bookName;
        this.bookPrice = bookPrice;
    }
     public String toString(){
        return id+"\t"+bookName+"\t"+bookPrice;
     }
}
