package collectionframework;

import java.util.HashMap;

public class HashMapDemo {
    public static void main(String[] args) {
        HashMap <Integer,String> data= new HashMap<>();
        data.put(1,"JAVA");
        data.put(2,"J2EE");
        data.put(3,"WEB-TECH");
        System.out.println(data);
    }
}
