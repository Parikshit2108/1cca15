package collectionframework;

import java.util.Scanner;

public class ArrayListDemo11 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        BookOperations b= new BookOperations();
        boolean status= true;
        while(status){
            System.out.println("Select mode of operation");
            System.out.println("1: Add Book");
            System.out.println("2: Display books");
            System.out.println("3: Remove Book");
            System.out.println("4: Exit");
            int choice = sc1.nextInt();
            switch (choice){
                case 1:
                    System.out.println("Enter Book id");
                    int id= sc1.nextInt();
                    System.out.println("Enter BooK name");
                    String name= sc1.next();
                    System.out.println("Enter Book Price");
                    double price= sc1.nextDouble();
                    b.addBook(id,name,price);
                    break;
                case 2:
                    b.displayBook();
                    break;
                case 3:
                    System.out.println("Enter Book id");
                    int bookId = sc1.nextInt();
                    b.removeBook(bookId);
                    break;
                case 4:
                    status=false;
                    break;

            }

        }
    }
}
