package collectionframework;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

public class LinkedHashMapDemo {
    public static void main(String[] args) {
        LinkedHashMap<String,Integer> data=new LinkedHashMap<>();
        data.put("Parikshit",21);
        data.put("Mayur",13);
        data.put("parikshit",23);

        System.out.println(data);
        Set<String> keys=data.keySet();
        System.out.println(keys);

        Collection<Integer> info= data.values();
        System.out.println(info);

        Set<Map.Entry<String, Integer>> entries =data.entrySet();
        System.out.println("Name\t\t\t date");
        System.out.println("--------------------------");
        for(Map.Entry<String,Integer> m:entries){
            System.out.println(m.getKey()+"\t\t\t"+m.getValue());
        }
    }
}
