package collectionframework;
import java.util.ArrayList;
import java.util.Iterator;
public class BookOperations {
    ArrayList<Book>bookList=new ArrayList<>();
    public void addBook( int id , String name,double price){
        Book b1= new Book(id ,name,price);
        bookList.add(b1);
        System.out.println("Book Added Successfully");
    }
    public void displayBook() {
        System.out.println("ID\tBOOK NAME\tBOOK PRICE");
        for(Book a:bookList){
            System.out.println(a);
        }
    }
    public void removeBook(int bookId) {
        Iterator<Book> itr = bookList.iterator();
            while(itr.hasNext()){
                if (itr.next().id==bookId){
                    itr.remove();
                    System.out.println("Book SuccessFully Removed");
                }
        }
    }
}
