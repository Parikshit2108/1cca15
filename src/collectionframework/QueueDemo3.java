package collectionframework;

import java.util.PriorityQueue;

public class QueueDemo3 {
    public static void main(String[] args) {
        PriorityQueue<Integer>  data= new PriorityQueue<>();
        data.offer(96);
        data.offer(12);
        data.offer(68);
        data.offer(67);
        data.offer(100);

        System.out.println(data);

    }
}
