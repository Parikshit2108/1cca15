package Interfaces;

public class MainApp2 {
    public static void main(String[] args) {
        CreditCard credit ;
        credit = new Visa();
        credit.getInfo();
        credit.withdraw(2500);
        System.out.println("=============================");
        credit= new Master();
        credit.getInfo();
        credit.withdraw(3500);

    }
}
