package Interfaces;

public interface Wallet {
    void makeBillPayment(double amt);
}
