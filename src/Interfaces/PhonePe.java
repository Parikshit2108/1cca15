package Interfaces;

public class PhonePe implements UPI,Wallet{
    @Override
    public void transferAmount(double amt) {
        System.out.println("Amount Transfer"+amt);
    }

    @Override
    public void makeBillPayment(double amt) {
        System.out.println("MakeBill Payment of Rs"+ amt);
    }
}
