package Interfaces;

public class Software extends FrontEnd implements BackEnd,Database{

    @Override
    public void developeServerProgram(String language) {
        System.out.println("Develope Server Program using "+language);
    }

    @Override
    public void developeDatabase(String language) {
        System.out.println("Desining DataBase Using "+ language);

    }
}
