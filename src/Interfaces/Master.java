package Interfaces;

public class Master implements CreditCard{
    @Override
    public void getInfo() {
        System.out.println("Card Type is Master");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction Successful");
    }
}
