package Interfaces;

public interface Demo {
    int k=20; //public static final variables
    void info();// non static abstract method
    static void test(){// static concrete method
        System.out.println("Test Method");
    }
}
