package Interfaces;

public class Visa implements CreditCard{
    @Override
    public void getInfo() {
        System.out.println("Card Type is Visa");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction Successful");
    }
}
