package Interfaces;

public interface CreditCard {
    void getInfo();
    void withdraw(double amt);
}
