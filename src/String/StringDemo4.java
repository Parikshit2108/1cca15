package String;

import java.util.Scanner;

public class StringDemo4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter The Name");
        String name= sc1.nextLine();
        String newName=name.trim();
        if(!newName.isEmpty()){
            System.out.println("Welcome\t"+ newName);
        }
        else {
            System.out.println("Invalid Name");
        }
    }
}
