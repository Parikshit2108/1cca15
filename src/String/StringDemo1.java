package String;

import java.util.Scanner;

public class StringDemo1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter The Name");
        String name = sc1.next();
        char[] data = name.toCharArray();
        for (int a= data.length-1; a>=0; a--) {
            System.out.println(data[a]);
        }
    }
}
