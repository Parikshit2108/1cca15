package String;

import java.text.BreakIterator;
import java.util.Scanner;

public class StringDemo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter The Name");
        String name= sc1.next();
        String name1=name.toLowerCase();
        char[] data= name1.toCharArray();
        int vCount=0 ;
        int cCount=0;
        for (char datum : data) {
            if (datum == 'a' || datum == 'i' || datum == 'e' || datum == 'o' || datum == 'u') {
                vCount++;
            } else {
                cCount++;
            }
        }
        System.out.println("Number of vowels in name\t"+ vCount);
        System.out.println("number of consonant in name\t"+ cCount);
    }
   /* int choice =2;
    switch(choice){
        case 1:
        System.out.println("false");
        break;
        case 2:
        System.out.println("true");
        break;
        default:
            System.out.println("invalid choice");*/

}
