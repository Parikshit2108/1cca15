package arrayList;
import java.util.ArrayList;
import java.util.Collections;

public class ArrayLists {
    public static void main(String[] args) {
        ArrayList<Integer> list= new ArrayList<>();
        //add elemnt
        list.add(0);
        list.add(2);
        list.add(3);
        System.out.println(list);
        //getElements
        int element = list.get(0);
        System.out.println(element);
        //add element btn
        list.add(1,1);
        System.out.println(list);

        //set element;
        list.set(0,5);
        System.out.println(list);
        //delet element
        list.remove(3);
        System.out.println(list);
        //size
        int size=list.size();
        System.out.println(size);

        //loopes
        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println();
        //sorting
        Collections.sort(list);
        System.out.println(list);
    }
}
