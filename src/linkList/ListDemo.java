package linkList;

public class ListDemo {
    private  int size;

    public ListDemo() {
        this.size =0;
    }

    class  Node {
        String data;
        Node next;
        Node (String data){
            this.data=data;
            this.next=null;
            size++;
        }
    }
    Node head=null;
    //add-first,and add last
    public void  addFirst(String data){
        Node newNode= new Node(data);
        if (head==null){
           head=newNode;
            return;
        }
        newNode.next=head;
        head= newNode;
    }
    public void addLast(String data){
        Node newNode= new Node(data);
        if (head==null){
            head=newNode;
            return;
        }
        Node currNode=head;
        while(currNode.next!=null){
            currNode=currNode.next;
        }
        currNode.next=newNode;

    }//print
    public void printList(){
        if (head==null){
            System.out.println("list is empty");
            return;
        }
        Node currNode=head;
        while(currNode!=null){
            System.out.print(currNode.data+"-->");
            currNode=currNode.next;
        }
        System.out.println("Null");

    }//deletfirst
    public void  deleteFirst(){
        if (head==null){
            System.out.println("The List is empty");
            return;
        }
        size--;
        head=head.next;
    }
    public void deleteLast(){
        if(head==null){
            System.out.println("this list is empty");
            return;
        }
        size--;
        Node  secondLast= head;
        Node lastNode=head.next;
        while(lastNode.next!=null){
            lastNode=lastNode.next;
            secondLast=secondLast.next;
        }
        secondLast.next=null;
    }

    public int getSize() {
        return size;
    }

    public static void main(String[] args) {
        ListDemo list=new ListDemo();
        list.addFirst("a");
        list.addFirst("is");
        list.printList();

        list.addLast("list");
        list.printList();

        list.addFirst("This");
        list.printList();

        list.deleteFirst();
        list.printList();

        list.deleteLast();
        list.printList();
        System.out.println(list.getSize());
        list.addFirst("This");
        System.out.println(list.getSize());

    }

    }

