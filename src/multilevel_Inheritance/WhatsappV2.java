package multilevel_Inheritance;

public class WhatsappV2 extends WhatsappV1 {
    // super class for whatsappv3 class and subclass of whatsapp1
    void calling(){
        System.out.println("make a call and receive a call");
    }
}
