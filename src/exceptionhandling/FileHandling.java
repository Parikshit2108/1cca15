package exceptionhandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandling {
    public static void main(String[] args){
        String msg= "Java is object oriented programing language";
        FileWriter fw=null;
        try {
            fw=new FileWriter("Demo.txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
