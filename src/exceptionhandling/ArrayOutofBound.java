package exceptionhandling;

public class ArrayOutofBound {
    public static void main(String[] args) {
        Object[] arr;
        arr = new String[2];
        arr[0]="abc";
        arr[1]= 50;
        arr[2]=4;
        for(Object a:arr){
            System.out.println(a);
        }
    }

}
