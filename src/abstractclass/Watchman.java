package abstractclass;

public class Watchman extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Desognation is Watchman");
    }

    @Override
    void getSallary() {
        System.out.println("Sallary is 40000");
    }
}
