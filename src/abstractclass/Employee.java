package abstractclass;

public abstract class Employee {
    abstract  void getDesignation(); // SuperClass
    abstract void getSallary();
}
