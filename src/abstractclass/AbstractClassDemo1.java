package abstractclass;

public abstract class AbstractClassDemo1 {
    static int k=20;
    double d=30.25;
    abstract void info();  // non-static abstract method
    void display(){
        System.out.println("Display method"); // non-static concrete  method
    }
    static void test(){
        System.out.println("Test method"); // static concrete method
    }

    public AbstractClassDemo1() {
        System.out.println("Abstract class constructor");
    }
    static {
        System.out.println("Static block");
    }
    {
        System.out.println("Non-static block");
    }
}
