package abstractclass;

public class Manager extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is Manager");
    }

    @Override
    void getSallary() {
        System.out.println("Sallary is 150000");

    }
}
