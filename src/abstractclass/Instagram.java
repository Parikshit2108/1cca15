package abstractclass;

public abstract class Instagram {
    void makePost(){
        System.out.println("Post photo or video");
    }
    abstract  void stories();
    abstract  void reels();
}
